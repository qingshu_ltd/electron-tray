const electron = require('electron');
const { app, Menu, Tray } = electron;
const { BrowserWindow } = electron;
const path = require('path');

let win;
let tray = null
function createWindow() {
  // 创建窗口并加载页面
  win = new BrowserWindow({width: 800, height: 600, icon: path.join(__dirname, 'icon.ico')});
  win.loadURL(`file://${__dirname}/index.html`);

  // 打开窗口的调试工具
  win.webContents.openDevTools();
  win.setMenu(null)
  // 窗口关闭的监听  
  win.on('closed', (event) => {
    win = null;
  });
  win.on('close', (event) => {  
	  win.hide();
	  win.setSkipTaskbar(true);
	  event.preventDefault();
  });
  win.on('show', () => {
    tray.setHighlightMode('always')
  })
  win.on('hide', () => {
    tray.setHighlightMode('never')
  })
  tray = new Tray(path.join(__dirname, 'icon.ico'));
   const contextMenu = Menu.buildFromTemplate([
      {label: '退出', click: () => {win.destroy()}},
    ])
    tray.setToolTip('My托盘测试')
    tray.setContextMenu(contextMenu)
	tray.on('click', ()=>{
		win.isVisible() ? win.hide() : win.show()
		win.isVisible() ?win.setSkipTaskbar(false):win.setSkipTaskbar(true);
	})
}

app.on('ready', createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit(); 	
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});


